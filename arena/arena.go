package arena

import (
	"github.com/romuloalves/distributed-systems-mk-server/player"
)

// Arena is the arena to combat
type arena struct {
	players map[string]player.Player
}

// InitializeArena returns a new arena will no players
func initializeArena() *arena {
	myArena := new(arena)
	myArena.players = make(map[string]player.Player)
	return myArena
}

// setPlayer includes a new player in the arena
func (g *arena) setPlayer(p player.Player) {
	g.players[p.ID] = p
}

// getPlayer returns the player from the arena
func (g *arena) getPlayer(playerID string) player.Player {
	return g.players[playerID]
}

// getAllPlayer returns all players from the arena
func (g *arena) getAllPlayer() map[string]player.Player {
	return g.players
}
