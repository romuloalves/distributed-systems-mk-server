package arena

import (
	"math/rand"
	"net"
	"strconv"
	"sync"

	"fmt"

	"math"

	"github.com/romuloalves/distributed-systems-mk-server/player"
)

const (
	maxPlayersPerArena = 2
)

var mutex = &sync.Mutex{}

// Manager will manage all arenas
type Manager struct {
	arenas map[string]arena
}

// InitializeManager returns a new manager
func InitializeManager() *Manager {
	manager := new(Manager)
	manager.arenas = make(map[string]arena)
	return manager
}

// AddPlayerInArena will add the player in a arena that isn't full
func (m *Manager) AddPlayerInArena(conn net.Conn) (string, string) {
	var p player.Player
	mutex.Lock()
	var playerArena arena
	var arenaID = strconv.Itoa(rand.Int())
	if len(m.arenas) == 0 {
		playerArena = *initializeArena()
	} else {
		gotArena := false
		for arenaDataID, arenaData := range m.arenas {
			playerLen := 0
			for _, player := range arenaData.players {
				if player.Online {
					playerLen++
				}
			}

			if playerLen < maxPlayersPerArena {
				playerArena = arenaData
				arenaID = arenaDataID
				gotArena = true
				break
			}
		}
		if !gotArena {
			playerArena = *initializeArena()
		}
	}

	playerID := strconv.Itoa(len(playerArena.players) + 1)
	p = player.CreateNewPlayer(conn, playerID)

	playerArena.players[p.ID] = p
	m.arenas[arenaID] = playerArena

	defer mutex.Unlock()

	fmt.Printf("> Player %s added to arena %s\n", p.ID, arenaID)

	return arenaID, playerID
}

// GetPlayerFromArena returns a player from an arena
func (m *Manager) GetPlayerFromArena(arenaID, playerID string) player.Player {
	mutex.Lock()
	arena := m.arenas[arenaID]
	defer mutex.Unlock()
	return arena.players[playerID]
}

// SetPlayerInArena sets a player in an arena
func (m *Manager) SetPlayerInArena(arenaID string, p player.Player) {
	mutex.Lock()
	m.arenas[arenaID].players[p.ID] = p
	defer mutex.Unlock()
}

// RemovePlayerFromArena remove a player
func (m *Manager) RemovePlayerFromArena(arenaID, playerID string) {
	mutex.Lock()
	player := m.arenas[arenaID].players[playerID]
	if !player.Online {
		player.OfflineMessageSent = true
		m.arenas[arenaID].players[playerID] = player
	}
	defer mutex.Unlock()
}

// GetAllArenas returns an arena and its players
func (m *Manager) GetAllArenas() map[string][]player.Player {
	mutex.Lock()
	arenas := make(map[string][]player.Player)
	for arenaKey, arenaData := range m.arenas {
		players := make([]player.Player, 0)
		for _, playerData := range arenaData.players {
			if !playerData.Online && playerData.OfflineMessageSent {
				continue
			}
			players = append(players, playerData)
		}
		arenas[arenaKey] = players
	}
	defer mutex.Unlock()
	return arenas
}

// PlayerAttachInArena will create an attach of playerAttach in the arena
func (m *Manager) PlayerAttachInArena(arenaID string, playerAttaching player.Player) player.Player {
	mutex.Lock()
	arena := m.arenas[arenaID]

	// Verify if players are in the range of position
	for _, player := range arena.players {
		if !player.Online || player.ID == playerAttaching.ID {
			continue
		}
		distance := getDistanceBetweenPlayers(playerAttaching, player)
		fmt.Printf("\nDistance between players %s and %s: %f", playerAttaching.ID, player.ID, distance)
		if distance < 10000 {
			playerAttaching.AttachWithSuccess()
		}
	}

	fmt.Printf("> Player %s with %f points\n", playerAttaching.ID, playerAttaching.Points)

	defer mutex.Unlock()

	return playerAttaching
}

func getDistanceBetweenPlayers(playerA, playerB player.Player) float64 {
	var distanceX float64
	var distanceY float64

	if playerA.Pos.X > playerB.Pos.X {
		distanceX = float64(playerA.Pos.X - playerB.Pos.X)
	} else {
		distanceX = float64(playerB.Pos.X - playerA.Pos.X)
	}

	if playerA.Pos.Y > playerB.Pos.Y {
		distanceY = float64(playerA.Pos.Y - playerB.Pos.Y)
	} else {
		distanceY = float64(playerB.Pos.Y - playerA.Pos.Y)
	}

	return math.Sqrt(math.Exp2(distanceX) + math.Exp2(distanceY))
}
