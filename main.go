package main

import (
	"net"
	"os"

	"github.com/romuloalves/distributed-systems-mk-server/arena"

	"bufio"

	"time"

	"strings"

	"github.com/fatih/color"
)

// Default configuration of the server
const (
	printFormat      = "\n> %s"
	movingSpeed      = 8
	sendDataInterval = time.Millisecond * 30
)

type printColor func(format string, params ...interface{})

// PrintlnColor will print a message in the terminal with a color
func PrintlnColor(message string, print printColor) {
	print(printFormat, message)
}

var gameManager arena.Manager

func main() {
	gameManager = *arena.InitializeManager()

	// Start listening
	listener, err := net.Listen("tcp", ":3000")
	if err != nil {
		PrintlnColor(err.Error(), color.Red)
		os.Exit(1)
	}
	PrintlnColor("Server started. Waiting connections...", color.Green)
	defer listener.Close()

	go sendDataToPlayers()

	for {
		// Receiving a new connection
		conn, err := listener.Accept()
		if err != nil {
			PrintlnColor(err.Error(), color.Red)
			continue
		}

		// Listen players
		handleNewPlayerRequest(conn)
	}
}

func handleNewPlayerRequest(connection net.Conn) {
	// Create a new player and add it to a arena
	arenaID, playerID := gameManager.AddPlayerInArena(connection)

	// Waiting for player moves
	go calculatingPlayerMoves(connection, playerID, arenaID)
}

func calculatingPlayerMoves(connection net.Conn, playerID, arenaID string) {
	moveUp := false
	moveDown := false
	moveRight := false
	moveLeft := false
	attach := false

	move := true

	connectionBuffer := bufio.NewReader(connection)
	// Receiving new message
	channelMessage := make(chan string)
	go receiveMessage(connectionBuffer, channelMessage)

	ticketInterval := time.NewTicker(sendDataInterval)

	for range ticketInterval.C {
		var message string
		select {
		case msg := <-channelMessage:
			message = msg
			go receiveMessage(connectionBuffer, channelMessage)
		default:
			message = ""
		}

		pl := gameManager.GetPlayerFromArena(arenaID, playerID)

		switch message {
		case "PR-U":
			moveUp = true
			moveDown = false
		case "PR-D":
			moveDown = true
			moveUp = false
		case "PR-R":
			moveRight = true
			moveLeft = false
		case "PR-L":
			moveLeft = true
			moveRight = false
		case "RE-U":
			moveUp = false
		case "RE-D":
			moveDown = false
		case "RE-R":
			moveRight = false
		case "RE-L":
			moveLeft = false
		case "AT":
			attach = true
			PrintlnColor("Player "+playerID+" attached in arena "+arenaID, color.Red)
		case "EXIT":
			PrintlnColor("Player "+playerID+" disconnected in arena "+arenaID, color.Red)
			pl.Online = false
			move = false
		}

		if move {
			if moveUp {
				pl.MoveUp(movingSpeed)
			}
			if moveDown {
				pl.MoveDown(movingSpeed)
			}
			if moveRight {
				pl.MoveRight(movingSpeed)
			}
			if moveLeft {
				pl.MoveLeft(movingSpeed)
			}

			if attach {
				pl = gameManager.PlayerAttachInArena(arenaID, pl)
				attach = false
			}
		}

		gameManager.SetPlayerInArena(arenaID, pl)
	}
}

func receiveMessage(r *bufio.Reader, channelMessage chan<- string) {
	message, _ := r.ReadString('\n')
	message = strings.TrimSpace(message)
	channelMessage <- message
}

// Send the position data to all users
func sendDataToPlayers() {
	ticketInterval := time.NewTicker(sendDataInterval)
	for range ticketInterval.C {
		arenas := gameManager.GetAllArenas()
		for arenaID, players := range arenas {
			for _, player := range players {
				var currentPlayerInformations string
				if !player.Online {
					currentPlayerInformations = player.ID + "_EXIT\n"
					gameManager.RemovePlayerFromArena(arenaID, player.ID)
				} else {
					currentPlayerInformations = player.GetPosition() + "\n"
					player.Conn.Write([]byte(currentPlayerInformations))
				}

				currentInformationBytes := []byte("NP_" + currentPlayerInformations)
				// Send data to all users that isnt the current
				for _, otherPlayer := range players {
					if otherPlayer.ID == player.ID {
						continue
					}
					otherPlayer.Conn.Write(currentInformationBytes)
				}
			}
		}
	}
}
