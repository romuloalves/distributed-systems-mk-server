package player

import (
	"testing"
)

const (
	speed = 10
)

func TestCreateNewPlayer(t *testing.T) {
	player := CreateNewPlayer(nil)
	if player.Pos.X != 0 {
		t.Fatal("Position X of the new player needs to be 0.")
	}
	if player.Pos.Y != 0 {
		t.Fatal("Position Y of the new player needs to be 0.")
	}
	if player.Pos.Direction != "R" {
		t.Fatal("Direction of the new player needs to be to Right.")
	}
	if player.Points != 0 {
		t.Fatal("Points of the new player needs to be 0.")
	}
}

func TestMovePlayerUp(t *testing.T) {
	player := CreateNewPlayer(nil)
	if player.Pos.Y != 0 {
		t.Fatal("Position Y of the new player needs to be 0.")
	}
	player.MoveUp(speed)
	if player.Pos.Y != 10 {
		t.Fatal("Position Y of the player, after move up needs to be 10.")
	}
}

func TestMovePlayerDown(t *testing.T) {
	player := CreateNewPlayer(nil)
	if player.Pos.Y != 0 {
		t.Fatal("Position Y of the new player needs to be 0.")
	}
	player.MoveDown(speed)
	if player.Pos.Y != -10 {
		t.Fatal("Position Y of the player, after move down needs to be -10.")
	}
}

func TestMovePlayerRight(t *testing.T) {
	player := CreateNewPlayer(nil)
	if player.Pos.X != 0 {
		t.Fatal("Position X of the new player needs to be 0.")
	}
	player.MoveRight(speed)
	if player.Pos.X != 10 {
		t.Fatal("Position X of the player, after move right needs to be 10.")
	}
	if player.Pos.Direction != "R" {
		t.Fatal("Direction of the player, after move right needs to be to right.")
	}
}

func TestMovePlayerLeft(t *testing.T) {
	player := CreateNewPlayer(nil)
	if player.Pos.X != 0 {
		t.Fatal("Position X of the new player needs to be 0.")
	}
	player.MoveLeft(speed)
	if player.Pos.X != -10 {
		t.Fatal("Position X of the player, after move left needs to be -10.")
	}
	if player.Pos.Direction != "L" {
		t.Fatal("Direction of the player, after move left needs to be to left.")
	}
}

func TestPlayerAttachedWithSuccess(t *testing.T) {
	player := CreateNewPlayer(nil)
	if player.Points != 0 {
		t.Fatal("Points of the new player needs to be 0.")
	}
	player.AttachWithSuccess()
	if player.Points != 1 {
		t.Fatal("Points of the player that attached with success needs to be 1.")
	}
}
