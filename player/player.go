package player

import "net"
import "strconv"
import "strings"

const (
	directionLeft  = "L"
	directionRight = "R"
)

// Position represents the position X and Y of the player
type Position struct {
	X         int
	Y         int
	Direction string
}

// Player represents an user, a player
type Player struct {
	ID                 string
	Pos                Position
	Points             int
	Conn               net.Conn
	Online             bool
	OfflineMessageSent bool
}

// GetPosition returns a string with all player informations joined with an underscore
func (p Player) GetPosition() string {
	return strings.Join([]string{p.ID, "_", strconv.Itoa(p.Pos.X), "_", strconv.Itoa(p.Pos.Y), "_", p.Pos.Direction, "_", strconv.Itoa(p.Points)}, "")
}

// MoveUp move the character to the up side
func (p *Player) MoveUp(speed int) {
	p.Pos.Y -= speed
}

// MoveRight move the character to the right side
func (p *Player) MoveRight(speed int) {
	p.Pos.X += speed
	p.Pos.Direction = directionRight
}

// MoveDown move the character to the down side
func (p *Player) MoveDown(speed int) {
	p.Pos.Y += speed
}

// MoveLeft move the character to the left side
func (p *Player) MoveLeft(speed int) {
	p.Pos.X -= speed
	p.Pos.Direction = directionLeft
}

// AttachWithSuccess give point to player because of his succeeded attach
func (p *Player) AttachWithSuccess() {
	p.Points++
}

// CreateNewPlayer will instantiate a new player to the game
func CreateNewPlayer(conn net.Conn, id string) Player {
	return Player{
		ID:                 id,
		Conn:               conn,
		Online:             true,
		OfflineMessageSent: false,
		Points:             0,
		Pos: Position{
			X:         50,
			Y:         50,
			Direction: "R",
		}}
}
